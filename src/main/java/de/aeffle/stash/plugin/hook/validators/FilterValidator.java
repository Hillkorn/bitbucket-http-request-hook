/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook.validators;


import com.atlassian.bitbucket.setting.Settings;

import java.util.regex.PatternSyntaxException;

public class FilterValidator extends Validator {
    public FilterValidator(Settings settings) {
        super(settings);
    }

    @Override
    public boolean hasErrors() {
        int locationCount = getLocationCount();
        boolean hasErrors = false;

        for (int id = 1; id <= locationCount; id++) {
            boolean isValidBranchFilter = validateBranchFilter(id);
            boolean isValidTagFilter = validateTagFilter(id);
            boolean isValidUserFilter = validateUserFilter(id);

            if (!(isValidBranchFilter && isValidTagFilter && isValidUserFilter)) {
                hasErrors = true;
            }
        }

        return hasErrors;
    }

    private boolean validateBranchFilter(int id) {
        return  validateFilter("branchFilter", id);
    }

    private boolean validateTagFilter(int id) {
        return  validateFilter("tagFilter", id);
    }

    private boolean validateUserFilter(int id) {
        return  validateFilter("userFilter", id);
    }

    private boolean validateFilter(String branchFilterName, int id) {

        if (validateRegexData(getFilterData(branchFilterName, id))) {
            return true;
        } else {
            String name = (id > 1 ? branchFilterName + id : branchFilterName);
            addError(name, "RegEx has a syntax error.");
            return false;
        }
    }

    private String getFilterData(String filterName, int id) {
        String dataName = (id > 1 ? filterName + id : filterName);
        return settings.getString(dataName, "");
    }

    private boolean validateRegexData(String data) {
        try {
            "".matches(data);
        } catch (PatternSyntaxException e) {
            return false;
        }
        return true;
    }


}
