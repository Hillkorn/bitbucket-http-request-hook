/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Properties;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.hook.repository.*;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.server.StorageService;
import com.atlassian.bitbucket.setting.Settings;

import de.aeffle.stash.plugin.hook.http.location.HttpLocation;
import de.aeffle.stash.plugin.hook.filter.ContextFilter;
import de.aeffle.stash.plugin.hook.http.agent.HttpAgent;
import de.aeffle.stash.plugin.hook.http.location.UrlTemplateTranslator;
import de.aeffle.stash.plugin.hook.http.location.SettingsHttpLocationReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Component;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;

import javax.annotation.Nonnull;

// @Log4j
@Component
public class HttpRequestPostReceiveHook implements PostRepositoryHook<RepositoryHookRequest> {
    private static final Logger log = LoggerFactory.getLogger(HttpRequestPostReceiveHook.class);

    private final ApplicationPropertiesService applicationPropertiesService;
    private final AuthenticationContext authenticationContext;
    private final CommitService commitService;
    private final StorageService storageService;
    private final int tabCount;

    @Inject
    public HttpRequestPostReceiveHook(@ComponentImport ApplicationPropertiesService applicationPropertiesService,
                                      @ComponentImport AuthenticationContext authenticationContext,
                                      @ComponentImport CommitService commitService,
                                      @ComponentImport StorageService storageService)
    {
        this.applicationPropertiesService = applicationPropertiesService;
        this.authenticationContext = authenticationContext;
        this.commitService = commitService;
        this.storageService = storageService;


        this.tabCount = getTabCountFromPropertiesFile(storageService);
    }

    private int getTabCountFromPropertiesFile(StorageService storageService) {
        final File configDir = storageService.getConfigDir().toFile();
        final String configFileName = "http-request-post-receive-hook.properties";
        final File propertyFile = new File(configDir, configFileName);

        int tabCount = 10;
        try {
            FileInputStream fileInputStream = new FileInputStream(propertyFile);
            Properties p = new Properties();
            p.load(fileInputStream);
            String tabCountString = p.getProperty("tab.count");
            tabCount = Integer.parseInt(tabCountString);
            if (tabCount < 10) {
                tabCount = 10;
            }
        } catch (FileNotFoundException e) {
            log.info("Config file " + configFileName + " not found - using default values");
        } catch (IOException e) {
            log.error("Problem reading from config file " + configFileName, e);
        }
        log.info("TabCount configured to " + tabCount);
        return tabCount;
    }



    @Override
    public void postUpdate(@Nonnull PostRepositoryHookContext postRepositoryHookContext,
                           @Nonnull RepositoryHookRequest repositoryHookRequest)
    {
        Settings settings = postRepositoryHookContext.getSettings();
        Collection<HttpLocation> httpLocations = new SettingsHttpLocationReader(settings).getAllFromContext();

        log.debug("Http Get Post Receive Hook started.");
        log.debug("User: " + authenticationContext.getCurrentUser().getName());
        log.debug("Number of HttpLocations: " + httpLocations.size());

        Collection<RefChange> refChanges = repositoryHookRequest.getRefChanges();
        for (RefChange refChange : refChanges) {
            log.debug("RefChange: " + refChange.getRef().getId());

            UrlTemplateTranslator translator = new UrlTemplateTranslator(applicationPropertiesService,
                    authenticationContext,
                    commitService,
                    postRepositoryHookContext,
                    repositoryHookRequest,
                    refChange);

            ContextFilter contextFilter = new ContextFilter(authenticationContext, refChange);

            for (HttpLocation httpLocation : httpLocations) {
                log.debug("HttpLocation: " + httpLocation);
                HttpLocation httpLocationTranslated = translator.translate(httpLocation);
                log.debug("HttpLocationTranslated: " + httpLocationTranslated);

                if (contextFilter.isMatching(httpLocationTranslated)) {
                    try {
                        HttpAgent httpAgent = new HttpAgent(httpLocationTranslated);
                        httpAgent.doRequest();
                    }
                    catch (Exception e) {
                        log.error("Problem calling " + httpLocationTranslated, e);
                    }
                }
            }
        }
    }
}
