package ut.de.aeffle.stash.plugin.hook.helper;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import lombok.extern.log4j.Log4j;
import org.apache.http.client.HttpClient;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.*;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.junit.Rule;
import org.junit.Test;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;


@Log4j
public class WebMockWithApacheHttpClient4SSLTest {
    private static final int CONNECTION_TIMEOUT_MILLIS = 500;
    private static final int SOCKET_TIMEOUT_MILLIS = 500;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().httpsPort(8443));


    @Test
    public void apacheHttpGetTest() throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        //GIVEN
        stubFor(get(urlEqualTo("/my"))
                .willReturn(aResponse()
                        .withStatus(200)));

        //WHEN
        SSLContext sslContext = SSLContextBuilder.create()
                                                 .loadTrustMaterial(null,
                                                         (TrustStrategy) (chain, authType) -> true)
                                                 .build();

        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", sslSocketFactory)
                .build();

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);

        HttpClient client = HttpClientBuilder.create()
                                             .setSSLContext(sslContext)
                                             .setConnectionManager(connectionManager)
                                             .build();

        int status = Executor.newInstance(client)
                             .execute(Request.Get("https://localhost:8443/my"))
                             .returnResponse()
                             .getStatusLine()
                             .getStatusCode();

        //THEN
        verify(1, getRequestedFor(urlEqualTo("/my")));
        assertThat(status).isEqualTo(200);
    }



    @Test
    public void httpsGetThrowsAnErrorWhenNotAcceptingCertificates() {
        //GIVEN
        stubFor(get(urlEqualTo("/my"))
                .willReturn(aResponse()
                        .withStatus(200)));

        //WHEN
        Throwable thrown = catchThrowable(() -> {
            Request.Get("https://localhost:8443/my").execute();
        });

        //THEN
        assertThat(thrown)
                .isInstanceOf(SSLHandshakeException.class)
                .hasMessageContaining("unable to find valid certification path to requested target");
    }

}
