/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.validators;

import com.atlassian.bitbucket.setting.Settings;
import de.aeffle.stash.plugin.hook.validators.UrlValidator;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


public class UrlValidatorTest {
 	private Settings settings;

	@Before
	public void beforeTestCreateCleanPlayground() {
		settings = mock(Settings.class);
	}

	private void setLocationCount(String count) {
		when(settings.getString("locationCount", "1")).thenReturn(count);
	}
	
	private void setUrl(int id, String url) {
		String urlName = ( id > 1 ? "url" + id : "url" );
		when(settings.getString(urlName, "")).thenReturn(url);
	}
	
	@Test
	public void testValidateWithEmptyUrl() {
		setLocationCount("1");
		setUrl(1, "");

        UrlValidator urlValidator = new UrlValidator(settings);

        assertThat(urlValidator.hasErrors()).isTrue();
	}

	
	@Test
	public void testValidateWithInvalidUrl() {
		setLocationCount("1");
		setUrl(1, "not a valid url");

        UrlValidator urlValidator = new UrlValidator(settings);

        assertThat(urlValidator.hasErrors()).isTrue();
	}
	
	@Test
	public void testValidateWithValidUrl() {
		setLocationCount("1");
		setUrl(1, "http://www.testing.com/valid.url");

        UrlValidator urlValidator = new UrlValidator(settings);

        assertThat(urlValidator.hasErrors()).isFalse();
	}
	
	@Test
	public void testValidateWithEmptyUrl2() {
		setLocationCount("2");
		setUrl(1, "");
		setUrl(2, "");

        UrlValidator urlValidator = new UrlValidator(settings);

        assertThat(urlValidator.hasErrors()).isTrue();
	}

	
	@Test
	public void testValidateWithInvalidUrl2() {
		setLocationCount("2");
		setUrl(1, "");
		setUrl(2, "not a valid url");

        UrlValidator urlValidator = new UrlValidator(settings);

        assertThat(urlValidator.hasErrors()).isTrue();
	}

	@Test
	public void testValidateWith2ValidUrl() {
		setLocationCount("2");
		setUrl(1, "http://www.testing.com/valid.url");
		setUrl(2, "http://www.testing.com/valid.url");

        UrlValidator urlValidator = new UrlValidator(settings);

        assertThat(urlValidator.hasErrors()).isFalse();
	}
	
	@Test
	public void testValidateWithEmptyUrl3() {
		setLocationCount("3");
		setUrl(1, "");
		setUrl(2, "");
		setUrl(3, "");

        UrlValidator urlValidator = new UrlValidator(settings);

        assertThat(urlValidator.hasErrors()).isTrue();
	}

	
	@Test
	public void testValidateWithInvalidUrl3() {
		setLocationCount("3");
		setUrl(1, "");
		setUrl(2, "");
		setUrl(3, "not a valid url");

        UrlValidator urlValidator = new UrlValidator(settings);

        assertThat(urlValidator.hasErrors()).isTrue();
	}

	@Test
	public void testValidateWith3ValidUrls() {
		setLocationCount("3");
		setUrl(1, "http://www.testing.com/valid.url");
		setUrl(2, "http://www.testing.com/valid.url");
		setUrl(3, "http://www.testing.com/valid.url");

        UrlValidator urlValidator = new UrlValidator(settings);

        assertThat(urlValidator.hasErrors()).isFalse();
	}
	

}
