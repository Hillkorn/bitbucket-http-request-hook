/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.testHelpers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.atlassian.bitbucket.repository.*;

import javax.annotation.Nonnull;

public class RefChangeMockFactory {
	private RefChange refChange;

	public RefChangeMockFactory() {
		clear();
	}

	public RefChangeMockFactory clear() {
		createNewMocks();
		loadDefaults();
		return this;
	}

	private void createNewMocks() {
		refChange = mock(RefChange.class);
	}

	private void loadDefaults() {
		setRefId("");
		setFromHash("");
		setToHash("");
		when(refChange.getType()).thenReturn(RefChangeType.UPDATE);
	}

    public RefChange getRefChange() {
		return refChange;
	}
	
	public void setRefId(String refId) {
		class Ref implements MinimalRef {
			private final String displayId;
			private final String id;
			private final RefType type;

			public Ref(String displayId, String id, RefType type) {
				this.displayId = displayId;
				this.id = id;
				this.type = type;
			}

			@Nonnull
			@Override
			public String getDisplayId() {
				return displayId;
			}

			@Nonnull
			@Override
			public String getId() {
				return id;
			}

			@Nonnull
			@Override
			public RefType getType() {
				return type;
			}
		}
		when(refChange.getRef()).thenReturn(new Ref(refId, refId, StandardRefType.BRANCH));
	}
	
	public void setToHash(String toHash) {
	    when(refChange.getToHash()).thenReturn(toHash);
    }

	public void setFromHash(String fromHash) {
        when(refChange.getFromHash()).thenReturn(fromHash);
    }
}
