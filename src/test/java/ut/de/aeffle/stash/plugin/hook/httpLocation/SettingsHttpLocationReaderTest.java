/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.httpLocation;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import de.aeffle.stash.plugin.hook.http.location.HttpLocation;
import org.junit.Before;
import org.junit.Test;

import ut.de.aeffle.stash.plugin.hook.testHelpers.RepositoryHookContextMockFactory;


public class SettingsHttpLocationReaderTest {
	private final RepositoryHookContextMockFactory repositoryHookContextFactory = new RepositoryHookContextMockFactory();

    @Before
    public void beforeTestClearSettings() {
    	repositoryHookContextFactory.clear();
    }

 
	@Test
	public void testGetUrl1() {
        //GIVEN
		repositoryHookContextFactory.setUrl( 1, "http://aeffle.de");

		//WHEN
        HttpLocation firstHttpLocation = repositoryHookContextFactory.getFirstHttpLocation();

		//THEN
        assertThat(firstHttpLocation.getUrlTemplate())
                .isEqualTo("http://aeffle.de");
	}

	@Test
	public void testGetUser1() {
        //GIVEN
		repositoryHookContextFactory.setUser(1, "john.doe");

		//WHEN
        HttpLocation firstHttpLocation = repositoryHookContextFactory.getFirstHttpLocation();

        //THEN
        assertThat(firstHttpLocation.getUser())
                .isEqualTo("john.doe");
	}

	@Test
	public void testGetPass1() {
        //GIVEN
		repositoryHookContextFactory.setPass(1, "secret");

		//WHEN
        HttpLocation firstHttpLocation = repositoryHookContextFactory.getFirstHttpLocation();

        //THEN
        assertThat(firstHttpLocation.getPass()).isEqualTo("secret");
	}

	@Test
	public void testGetUseAuthNullV1() {
        //GIVEN
		repositoryHookContextFactory.setOldUseAuth(null);

		//WHEN
        HttpLocation firstHttpLocation = repositoryHookContextFactory.getFirstHttpLocation();

		//THEN
        assertThat(firstHttpLocation.isAuthEnabled())
                .isFalse();
	}
	
	@Test
	public void testGetUseAuthFalseV1() {
		//GIVEN
        repositoryHookContextFactory.setOldUseAuth(false);

        //WHEN
        HttpLocation firstHttpLocation = repositoryHookContextFactory.getFirstHttpLocation();

        //THEN
        assertThat(firstHttpLocation.isAuthEnabled()).isFalse();
	}

	@Test
	public void testGetUseAuthTrueV1() {
        //GIVEN
		repositoryHookContextFactory.setOldUseAuth(true);

		//WHEN
		HttpLocation firstHttpLocation = repositoryHookContextFactory.getFirstHttpLocation();

		//THEN
		assertThat(firstHttpLocation.isAuthEnabled()).isTrue();
	}

	@Test
	public void testGetUseAuthTrueV2() {
        //GIVEN
		repositoryHookContextFactory.setVersion("2");
		repositoryHookContextFactory.setUseAuth(1, true);

		//WHEN
		HttpLocation firstHttpLocation = repositoryHookContextFactory.getFirstHttpLocation();

		//THEN
		assertThat(firstHttpLocation.isAuthEnabled()).isTrue();
	}
	
	@Test
	public void testgetNumberOfHttpLocationsNull() {
        //GIVEN
		repositoryHookContextFactory.setLocationCount(null);

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookContextFactory.getHttpLocations();

		//THEN
		assertThat(httpLocations.size()).isEqualTo(1);
   }
	
	@Test
	public void testgetNumberOfHttpLocations1() {
        //GIVEN
		repositoryHookContextFactory.setLocationCount("1");

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookContextFactory.getHttpLocations();

		//THEN
        assertThat(httpLocations.size()).isEqualTo(1);
   }
	
	@Test
	public void testgetNumberOfHttpLocations3() {
        //GIVEN
		repositoryHookContextFactory.setLocationCount("3");

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookContextFactory.getHttpLocations();

		//THEN
        assertThat(httpLocations.size()).isEqualTo(3);
    }


	@Test
	public void testGetUrl2() {
        //GIVEN
		repositoryHookContextFactory.setLocationCount("2");
		repositoryHookContextFactory.setUrl(2, "http://aeffle.de");

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookContextFactory.getHttpLocations();
		HttpLocation httpLocation = repositoryHookContextFactory.getHttpLocation(2);

		//THEN
		assertThat(httpLocations.size()).isGreaterThanOrEqualTo(2)
                .withFailMessage("LocationCount shouldn't be smaller than 2.");

		assertThat(httpLocation.getUrlTemplate()).isEqualTo("http://aeffle.de");
	}
	
	@Test
	public void testGetUser2() {
        //GIVEN
		repositoryHookContextFactory.setLocationCount("2");
		repositoryHookContextFactory.setUser(2, "john.doe");

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookContextFactory.getHttpLocations();
		HttpLocation httpLocation = repositoryHookContextFactory.getHttpLocation(2);

		//THEN
        assertThat(httpLocations.size()).isGreaterThanOrEqualTo(2)
                .withFailMessage("LocationCount shouldn't be smaller than 2.");

        assertThat(httpLocation.getUser()).isEqualTo("john.doe");
	}
	
	@Test
	public void testGetPass2() {
        //GIVEN
		repositoryHookContextFactory.setLocationCount("2");
		repositoryHookContextFactory.setPass(2, "secret");

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookContextFactory.getHttpLocations();
		HttpLocation httpLocation = repositoryHookContextFactory.getHttpLocation(2);

        //THEN
        assertThat(httpLocations.size()).isGreaterThanOrEqualTo(2)
                .withFailMessage("LocationCount shouldn't be smaller than 2.");

		assertThat(httpLocation.getPass()).isEqualTo("secret");
	}
	
	@Test
	public void testManyLocations() {
        //GIVEN
		repositoryHookContextFactory.setVersion("2");
		repositoryHookContextFactory.setLocationCount("3");
		
		repositoryHookContextFactory.setUrl( 1, "http://aeffle.de/1");
		repositoryHookContextFactory.setUrl( 2, "http://aeffle.de/2");
		repositoryHookContextFactory.setUrl( 3, "http://aeffle.de/3");

		repositoryHookContextFactory.setUseAuth(1, true);
		repositoryHookContextFactory.setUseAuth(2, true);
		repositoryHookContextFactory.setUseAuth(3, true);

		repositoryHookContextFactory.setUser(1, "john.doe1");
		repositoryHookContextFactory.setUser(2, "john.doe2");
		repositoryHookContextFactory.setUser(3, "john.doe3");

		repositoryHookContextFactory.setPass(1, "secret1");
		repositoryHookContextFactory.setPass(2, "secret2");
		repositoryHookContextFactory.setPass(3, "secret3");

		//WHEN
		List<HttpLocation> httpLocations = repositoryHookContextFactory.getHttpLocations();
		HttpLocation httpLocation1 = repositoryHookContextFactory.getHttpLocation(1);
		HttpLocation httpLocation2 = repositoryHookContextFactory.getHttpLocation(2);
		HttpLocation httpLocation3 = repositoryHookContextFactory.getHttpLocation(3);

        //THEN
		assertThat(httpLocations.size()).isGreaterThanOrEqualTo(3)
                .withFailMessage("LocationCount shouldn't be smaller than 3.");

		assertThat(httpLocation1.getUrlTemplate()).isEqualTo("http://aeffle.de/1");
		assertThat(httpLocation2.getUrlTemplate()).isEqualTo("http://aeffle.de/2");
		assertThat(httpLocation3.getUrlTemplate()).isEqualTo("http://aeffle.de/3");

		assertThat(httpLocation1.isAuthEnabled()).isTrue();
		assertThat(httpLocation2.isAuthEnabled()).isTrue();
		assertThat(httpLocation3.isAuthEnabled()).isTrue();

		assertThat(httpLocation1.getUser()).isEqualTo("john.doe1");
		assertThat(httpLocation2.getUser()).isEqualTo("john.doe2");
		assertThat(httpLocation3.getUser()).isEqualTo("john.doe3");

		assertThat(httpLocation1.getPass()).isEqualTo("secret1");
		assertThat(httpLocation2.getPass()).isEqualTo("secret2");
		assertThat(httpLocation3.getPass()).isEqualTo("secret3");
	}
}