/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.validators;

import com.atlassian.bitbucket.setting.Settings;
import de.aeffle.stash.plugin.hook.validators.FilterValidator;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;



public class FilterValidatorTest {

    private Settings settings;

    @Before
    public void beforeTestCreateCleanPlayground() {
        settings = mock(Settings.class);
    }

    private void setLocationCount(String count) {
        when(settings.getString("locationCount", "1")).thenReturn(count);
    }

    private void setBranchFilter(int id, String data) {
        setFilter(id, "branchFilter", data);
    }

    private void setTagFilter(int id, String data) {
        setFilter(id, "tagFilter", data);
    }

    private void setUserFilter(int id, String data) {
        setFilter(id, "userFilter", data);
    }

    private void setFilter(int id, String filter, String data) {
        String filterName = (id > 1 ? filter + id : filter);
        when(settings.getString(filterName, "")).thenReturn(data);
    }


    @Test
    public void allEmptyShouldNotFindErrors() {
        //GIVEN
        setLocationCount("1");
        setBranchFilter(1,"");
        setTagFilter(1, "");
        setUserFilter(1, "");

        //WHEN
        FilterValidator filterValidator = new FilterValidator(settings);

        //THEN
        assertThat(filterValidator.hasErrors()).isFalse();
    }


    @Test
    public void validRegExShouldNotFindErrors() {
        //GIVEN
        setLocationCount("1");
        setBranchFilter(1,"^a-to-z$");
        setTagFilter(1, "");
        setUserFilter(1, "");

        //WHEN
        FilterValidator filterValidator = new FilterValidator(settings);

        //THEN
        assertThat(filterValidator.hasErrors()).isFalse();
    }

    @Test
    public void inValidBranchRegExShouldReturnFalse() {
        //GIVEN
        setLocationCount("1");
        setBranchFilter(1,"*");
        setTagFilter(1, "");
        setUserFilter(1, "");

        //WHEN
        FilterValidator filterValidator = new FilterValidator(settings);

        //THEN
        assertThat(filterValidator.hasErrors()).isTrue();
    }

    @Test
    public void inValidUserRegExShouldReturnFalse() {
        //GIVEN
        setLocationCount("1");
        setBranchFilter(1,"");
        setTagFilter(1, "");
        setUserFilter(1, "*");

        //WHEN
        FilterValidator filterValidator = new FilterValidator(settings);

        //THEN
        assertThat(filterValidator.hasErrors()).isTrue();
    }
}